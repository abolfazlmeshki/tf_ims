module "eks" {
  count  = 0
  source = "git::https://gitlab.com/abolfazlmeshki/tf_aws_eks.git?ref=v0.6"
}

module "gke" {
  count  = 1
  source = "git::https://gitlab.com/abolfazlmeshki/gcp_gks.git?ref=v0.5"
}

module "helm" {
  depends_on = [
    module.eks,
    module.gke
  ]
  count = 1
  providers = {
    helm = helm.gke
  }
  source = "git::https://gitlab.com/abolfazlmeshki/tf_aws_helm.git?ref=v0.14"
}