terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.50.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.8.0"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.46.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "eu-central-1"
  alias  = "abal"
}

provider "google" {
  # Configuration options
  project     = "gcp-lab-359110"
  region      = "europe-west3"
  zone        = "europe-west3-a"
  credentials = "sa.json"
}

provider "helm" {
  # Configuration options
  alias = "eks"
  kubernetes {
    host                   = coalesce(try(module.eks[0].endpoint, null), try(module.gke[0].endpoint, null))
    cluster_ca_certificate = base64decode(coalesce(try(module.eks[0].cluster_ca_certificate, null), try(module.gke[0].cluster_ca_certificate, null)))
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", coalesce(try(module.eks[0].cluster_name, null), try(module.gke[0].cluster_name, null))]
      command     = "aws"
    }
  }
}

provider "helm" {
  # Configuration options
  alias = "gke"
  kubernetes {
    host                   = coalesce(try(module.eks[0].endpoint, null), try(module.gke[0].endpoint, null))
    cluster_ca_certificate = base64decode(coalesce(try(module.eks[0].cluster_ca_certificate, null), try(module.gke[0].cluster_ca_certificate, null)))
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "gke-gcloud-auth-plugin"
    }
  }
}