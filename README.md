<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.50.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | 4.46.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.8.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_eks"></a> [eks](#module\_eks) | git::https://gitlab.com/abolfazlmeshki/tf_aws_eks.git | v0.6 |
| <a name="module_gke"></a> [gke](#module\_gke) | git::https://gitlab.com/abolfazlmeshki/gcp_gks.git | v0.5 |
| <a name="module_helm"></a> [helm](#module\_helm) | git::https://gitlab.com/abolfazlmeshki/tf_aws_helm.git | v0.14 |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
<!-- END_TF_DOCS -->